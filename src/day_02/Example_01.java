package day_02;

public class Example_01 {

	public static void main(String[] args) {

		System.out.println("Main method() execution started here");

		// Validate given number is Even or Odd

		int input = 7;

		if (input % 2 == 0) {
			
			System.out.println("The given input "+input+" is Even number");

		} else {
			
			System.out.println("This given input "+input+ " is Odd Number");
		}

		System.out.println("Main method() execution stopped here ");

	}

}