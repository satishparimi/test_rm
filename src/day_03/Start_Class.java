package day_03;

public class Start_Class {

	public static void main(String[] args) {
		
		//Student Marks Verification
		
		int marks =  41 ;
		
		if(marks >= 40) {
			
			System.out.println("1. Person is passed and Marks are : "+marks);
			
			//Validate First Class- Students
			if(marks >= 60) {
				
				System.out.println("Person got : "+marks+", So He/She qualified with First Class ");
			}
			else if(marks >= 50 && marks < 60) {
				
				System.out.println("Person got : "+marks + ", so He/She qualified with second class");
			}
			else if(marks >=40 && marks <50) {
				System.out.println("Person got : "+marks+" , so He/She qualified with Thrid Class");
			}
			
			//Valid Condition : marks >=40 && marks <50
			
		}
		else {
			System.out.println("1. Person is Failed, Good Luck next time & Marks : "+marks);
		}

	}

}
