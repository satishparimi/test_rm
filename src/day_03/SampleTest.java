package day_03;

public class SampleTest {

	public static void main(String[] args) {

		// Pbm : Validate Person age is eligible for Job

		// Condition : Person age should be >= 18 and < 60

		int age = 62;

		// Nested If-else example
		if (age >= 18) {

			System.out.println("Person age is 18");

			if (age <= 60) {

				System.out.println("Person age " + age + " is < 60");

				if (age > 17) {

				} else {

				}

			} else {
				System.out.println("Person age is " + age + " > 60");
			}

		} else {

			System.out.println("Person age : " + age + " is < 18");
		}
	}

}
